package com.base.camerax.firebase.remoteconfig

import com.base.camerax.base.presenter.MvpView

interface RemoteConfigView : MvpView {

    fun onUpdateAppNeeded(forceUpdate: Boolean, message: String?)

    fun onUpdateBaseUrlNeeded(type: String?, url: String?)

}