package com.base.camerax.helper.permission

interface ResponsePermissionCallback {
     fun onResult(permissionResult: List<String>)
}