package com.base.camerax.helper

object CommonConstant {

    // for intent extras constant
    const val APP_CRASH = "app_crash"

    // -- Remote Config Params
    const val NOTIFY_NORMAL_UPDATE = "minimum_info_android"
    const val NOTIFY_FORCE_UPDATE = "minumum_force_android"
    const val NOTIFY_NORMAL_MESSAGE = "info_message"
    const val NOTIFY_FORCE_MESSAGE = "force_message"
    const val NEW_BASE_URL = "endpoint_url"
    const val CHECK_APP_VERSION = "app_version"
    const val CHECK_BASE_URL = "base_url"

}