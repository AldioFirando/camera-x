package com.base.camerax.data.local.prefs

class SuitException(message: String?) : RuntimeException(message)