package com.base.camerax.feature.main

import android.os.Bundle
import com.base.camerax.R
import com.base.camerax.base.ui.BaseActivity
import com.base.camerax.feature.camera.CameraActivity
import kotlinx.android.synthetic.main.activity_main.*

/**
 * Main entry point into our app. This app follows the single-activity pattern, and all
 * functionality is implemented in the form of fragments.
 */
class MainActivity : BaseActivity() {

    override val resourceLayout: Int = R.layout.activity_main

    override fun onViewReady(savedInstanceState: Bundle?) {
        takeCamera.setOnClickListener {
            goToActivity(CameraActivity::class.java, null, clearIntent = false, isFinish = true)
        }
    }

}
