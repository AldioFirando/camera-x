package com.base.camerax.feature.splashscreen

import com.base.camerax.base.presenter.MvpView

/**
 * Created by dodydmw19 on 12/19/18.
 */

interface SplashScreenView : MvpView {

    fun navigateToMainView()

}