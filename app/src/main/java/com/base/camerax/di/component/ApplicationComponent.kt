package com.base.camerax.di.component

import com.base.camerax.di.module.ApplicationModule
import com.base.camerax.di.scope.SuitCoreApplicationScope
import com.base.camerax.feature.login.LoginPresenter
import com.base.camerax.feature.splashscreen.SplashScreenPresenter
import com.base.camerax.firebase.remoteconfig.RemoteConfigPresenter
import com.base.camerax.onesignal.OneSignalPresenter
import dagger.Component

@SuitCoreApplicationScope
@Component(modules = [ApplicationModule::class])
interface ApplicationComponent {

    fun inject(loginPresenter: LoginPresenter)

    fun inject(splashScreenPresenter: SplashScreenPresenter)

    fun inject(oneSignalPresenter: OneSignalPresenter)

    fun inject(remoteConfigPresenter: RemoteConfigPresenter)
}