package com.base.camerax.di.module

import javax.inject.Qualifier

@Qualifier @Retention annotation class ApplicationContext
